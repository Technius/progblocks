extends PanelContainer

signal editor_deleted

func _ready():
	get_delete_button().connect("button_down", self, "_on_delete_pressed")

# Populates this panel with data from the command
func load_command(_cmd):
	pass

# Returns the command generated using data from this panel
func get_command():
	pass

# Returns the delete button
func get_delete_button():
	return $Layout/MarginContainer/Header/DeleteButton

func _on_delete_pressed():
	self.emit_signal("editor_deleted", self)
	self.queue_free()

# Enables or disables this panel
func set_locked(locked):
	get_delete_button().disabled = locked