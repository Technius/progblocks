extends Node

# class_name EditorUtil

# Creates a command editor for the given command type, or returns null if there
# is no such command type
static func create_command_editor(cmd_type: String):
	if cmd_type == "Move":
		return preload("res://ui/CMoveEditor.tscn").instance()
	elif cmd_type == "OnCollide":
		return preload("res://ui/COnCollideEditor.tscn").instance()
	elif cmd_type == "ChangeVariable":
		return preload("res://ui/CChangeVariableEditor.tscn").instance()
	elif cmd_type == "IfVariable":
		return preload("res://ui/CIfVariableEditor.tscn").instance()
	else:
		print("Unknown command type: ", cmd_type)
		return null