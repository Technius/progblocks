extends "CommandEditPanel.gd"

func _ready():
	pass

func load_command(cmd):
	assert(cmd.type == "OnCollide")
	assert(cmd.action != null)
	$Layout/EmbeddedCEditor.load_command(cmd.action)

func get_command():
	var gen_action = $Layout/EmbeddedCEditor.get_command()
	if gen_action == null:
		return null
	else:
		return {
			"type": "OnCollide",
			"action": gen_action
		}

func set_locked(locked):
	.set_locked(locked)
	$Layout/EmbeddedCEditor.set_locked(locked)