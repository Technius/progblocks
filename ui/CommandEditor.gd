extends WindowDialog

var command_list_ui

func _ready():
	command_list_ui = $MarginContainer/VBoxContainer/ScrollContainer/CommandList
	var controls = $MarginContainer/VBoxContainer/Controls
	controls.get_node("AddButton").connect("button_up", self, "_add_command_action")
	controls.get_node("ClearButton").connect("button_up",self, "load_commands", [{}])

func load_commands(commands):
	for c in command_list_ui.get_children():
		c.queue_free()
	
	for cmd in commands:
		var editor = EditorUtil.create_command_editor(cmd.type)
		if editor != null:
			editor.load_command(cmd)
			command_list_ui.add_child(editor)

func get_commands():
	var cl = []
	for c in command_list_ui.get_children():
		cl.append(c.get_command())
	return cl

func _add_command_action():
	var opt_btn = $MarginContainer/VBoxContainer/Controls/CommandSelect
	var cmd_type = opt_btn.get_item_text(opt_btn.selected)
	var editor = EditorUtil.create_command_editor(cmd_type)
	if editor != null:
		command_list_ui.add_child(editor)

func set_locked(locked):
	for c in command_list_ui.get_children():
		c.set_locked(locked)
	for c in $MarginContainer/VBoxContainer/Controls.get_children():
		if "disabled" in c:
			c.disabled = locked