extends "CommandEditPanel.gd"

func _ready():
	pass

func load_command(cmd):
	assert(cmd.type == "IfVariable")
	assert(cmd.action != null)
	assert(cmd.name != null)
	assert(cmd.criterion != null)
	assert(cmd.action != null)
	assert(cmd.value != null)
	$Layout/Name/Field.text = cmd.name
	$Layout/Criterion/Field.select(cmd.criterion)
	$Layout/Value/Field.text = str(cmd.value)
	$Layout/Action.load_command(cmd.action)

func get_command():
	var name = $Layout/Name/Field.text
	var criterion = $Layout/Criterion/Field.get_selected_id()
	var value = $Layout/Value/Field.text
	var gen_action = $Layout/Action.get_command()
	if name.match("  *"): # empty
		return null
	elif not value.is_valid_integer():
		return null
	elif gen_action == null:
		return null
	else:
		return {
			"type": "IfVariable",
			"name": name,
			"criterion": criterion,
			"value": int(value),
			"action": gen_action
		}

func set_locked(locked):
	.set_locked(locked)
	$Layout/Name/Field.editable = not locked
	$Layout/Criterion/Field.disabled = locked
	$Layout/Value/Field.editable = not locked
	$Layout/Action.set_locked(locked)