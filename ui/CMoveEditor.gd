extends "CommandEditPanel.gd"

const MAX_MOVEMENT = 5000

func _ready():
	pass

func load_command(cmd):
	assert(cmd.type == "Move")
	$Layout/HBoxContainer/EntryX.text = str(cmd.impulse.x)
	$Layout/HBoxContainer/EntryY.text = str(cmd.impulse.y)

func get_command():
	var val_x = $Layout/HBoxContainer/EntryX.text
	var val_y = $Layout/HBoxContainer/EntryY.text
	if not (val_x.is_valid_float() and val_y.is_valid_float()):
		return null

	val_x = float(val_x)
	val_y = float(val_y)

	# Place a maximum value on movement to avoid mechanics abuse
	if abs(val_x) > MAX_MOVEMENT or abs(val_y) > MAX_MOVEMENT:
		return null

	return {
		"type": "Move",
		"impulse": Vector2(val_x, val_y)
	}

func set_locked(locked):
	.set_locked(locked)
	$Layout/HBoxContainer/EntryX.editable = not locked
	$Layout/HBoxContainer/EntryY.editable = not locked