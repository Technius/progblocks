extends "CommandEditPanel.gd"

func _ready():
	pass

# Populates this panel with data from the command
func load_command(cmd):
	assert(cmd.type == "ChangeVariable")
	assert(cmd.name != null)
	assert(cmd.action != null)
	assert(cmd.value != null)
	$Layout/Name/Field.text = cmd.name
	$Layout/Action/Field.select(cmd.action)
	$Layout/Value/Field.text = str(cmd.value)

# Returns the command generated using data from this panel
func get_command():
	var name = $Layout/Name/Field.text
	var action = $Layout/Action/Field.get_selected_id()
	var value = $Layout/Value/Field.text
	if name.match("  *"): # empty
		return null
	if not value.is_valid_integer():
		return null
	return {
		"type": "ChangeVariable",
		"name": name,
		"action": action,
		"value": int(value)
	}

# Enables or disables this panel
func set_locked(locked):
	.set_locked(locked)
	$Layout/Name/Field.editable = not locked
	$Layout/Action/Field.disabled = locked
	$Layout/Value/Field.editable = not locked