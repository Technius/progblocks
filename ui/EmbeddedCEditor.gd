extends HBoxContainer

export(NodePath) var parent

var action = null
var action_editor = null

func _ready():
	$SetTypeButton.connect("button_up", self, "on_set_type_pressed")

func load_command(cmd):
	self.action = cmd
	self.make_action_editor(cmd.type)
	self.action_editor.load_command(self.action)

func get_command():
	if action_editor == null:
		return null
	var gen_action = action_editor.get_command()
	if gen_action == null:
		return null
	else:
		return gen_action

func on_action_editor_delete(ignored):
	self.visible = true

func make_action_editor(cmd_type):
	self.action_editor = EditorUtil.create_command_editor(cmd_type)
	self.action_editor.connect("editor_deleted", self, "on_action_editor_delete")
	get_node(parent).add_child(self.action_editor)
	self.visible = false

func on_set_type_pressed():
	var select_btn = $CommandSelect
	var cmd_type = select_btn.get_item_text(select_btn.selected)
	self.make_action_editor(cmd_type)

func set_locked(locked):
	action_editor.set_locked(locked)