extends Node2D

signal return_level_select

export(Resource) var level_resource = preload("levels/Level1.tscn")

var level
var marker

var commands = []
var var_table = {}

func _ready():
	reset_level()
	$LevelArea.connect("body_exited", self, "_object_exit_level")
	$GUI/Controls/ShowCmdListButton.connect("button_down", $GUI/CommandEditor, "show")
	$GUI/Controls/StartButton.connect("toggled", self, "toggle_sim")
	$GUI/Controls/LevelSelect.connect("button_down", self, "on_back_level_select_pressed")
	$GUI/WinPopup/Buttons/Replay.connect("button_down", $GUI/WinPopup, "hide")
	$GUI/WinPopup/Buttons/BackLevelSelect.connect("button_down", self, "on_back_level_select_pressed")
	$GUI/WinPopup.connect("popup_hide", self, "reset_level")
	
	$SimTimer.connect("timeout", self, "reset_level")

func _process(delta):
	update_elapsed_time_label($SimTimer.wait_time - $SimTimer.time_left)

func _marker_collide(other, body):
	for c in commands:
		if c.type == "OnCollide":
			run_command(c)
	if other.is_in_group("goal"):
		$GUI/WinPopup.popup()
		get_tree().paused = true

func _object_exit_level(body):
	if body == marker:
		reset_level()

# Command types:
# Move: { move: String, impulse: Vector2 }
# OnCollide: { action: Command }
# ChangeVariable: { name: String, action: index of ChangeVariable, value: int }
# IfVariable: { name: String, criterion: index of VarCriteria, action: Command }

# Command types that run on start
const startup_commands = ["Move", "ChangeVariable", "IfVariable"]

enum ChangeVariable { Set, Add }
enum VarCriteria { LT, EQ, GT }

func run_command(cmd):
	if cmd.type == "Move":
		marker.apply_impulse(Vector2(0, 0), Vector2(cmd.impulse.x, -cmd.impulse.y))
	elif cmd.type == "OnCollide":
		run_command(cmd.action)
	elif cmd.type == "ChangeVariable":
		$GUI/VariablesWindow.show()
		var new_value
		if cmd.action == ChangeVariable.Set:
			new_value = cmd.value
		elif cmd.action == ChangeVariable.Add:
			new_value = get_var(cmd.name) + cmd.value
		var_table[cmd.name] = new_value
		$GUI/VariablesWindow.update_value(cmd.name, new_value)
	elif cmd.type == "IfVariable":
		var value = get_var(cmd.name)
		var lt = cmd.criterion == VarCriteria.LT and value < cmd.value
		var eq = cmd.criterion == VarCriteria.EQ and value == cmd.value
		var gt = cmd.criterion == VarCriteria.GT and value > cmd.value
		if lt or eq or gt:
			run_command(cmd.action)
	else:
		print("Unrecognized action type: ", cmd.type)

func get_var(name):
	if var_table.has(name):
		return var_table[name]
	else:
		return 0

func start_level():
	var cmds = $GUI/CommandEditor.get_commands()
	commands = cmds
	for c in cmds:
		if c == null:
			$GUI/CommandErrorAlert.popup()
			return
		elif c.type in startup_commands:
			run_command(c)

	get_tree().paused = false
	$GUI/Controls/StartButton.pressed = true
	$GUI/CommandEditor.set_locked(true)
	if var_table.size() > 0:
		$GUI/VariablesWindow.show()
	$SimTimer.start()

func reset_level(load_default_commands=false):
	if level != null:
		level.queue_free()
	level = level_resource.instance()
	marker = level.get_node("Marker")
	$LevelParent.add_child(level)
	
	if load_default_commands and "default_commands" in level:
		commands.clear()
		for c in level.default_commands:
			commands.append(c)
	
	if "max_time" in level:
		$SimTimer.wait_time = level.max_time
	else:
		$SimTimer.wait_time = 10

	var_table.clear()
	$GUI/CommandEditor.load_commands(commands)

	marker.connect("body_entered", self, "_marker_collide", [level.get_node("Marker")])
	$SimTimer.stop()
	$GUI/Controls/StartButton.pressed = false
	update_elapsed_time_label(0)
	$GUI/CommandEditor.set_locked(false)
	$GUI/VariablesWindow.clear()
	get_tree().paused = true

func toggle_sim(pressed):
	if pressed:
		start_level()
	else:
		reset_level()

func update_elapsed_time_label(elapsed):
	$GUI/TimePanel/Label.text = str(elapsed) + " s"

func on_back_level_select_pressed():
	$GUI/WinPopup.hide()
	self.emit_signal("return_level_select")

func hide():
	.hide()
	$GUI/CommandEditor.hide()
	$GUI/VariablesWindow.hide()

func show():
	.show()
	$GUI/CommandEditor.show()