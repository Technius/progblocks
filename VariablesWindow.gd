extends WindowDialog

var bindings = {}

func _ready():
	pass

func update_value(name, value):
	if not bindings.has(name):
		var l = Label.new()
		self.add_child(l)
		bindings[name] = l
	bindings[name].text = name + " = " + str(value)
	$Scroll/Layout/NoVarLabel.visible = false

func clear():
	for n in bindings:
		self.remove_child(bindings[n])
	bindings.clear()
	$Scroll/Layout/NoVarLabel.visible = true