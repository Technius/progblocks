extends Node

var levels = [
	preload("levels/Level1.tscn"),
	preload("levels/Level2.tscn"),
	preload("levels/Level3.tscn"),
	preload("levels/Level4.tscn"),
	preload("levels/Level5.tscn"),
]

func _ready():
	$Main.hide()
	$Main.connect("return_level_select", self, "switch_to_level_select")
	var level_grid = $Panel/MarginContainer/VBoxContainer/GridContainer
	for i in range(0, levels.size()):
		var btn = Button.new()
		btn.set_text("Level " + str(i + 1))
		level_grid.add_child(btn)
		btn.connect("button_up", self, "switch_level", [i])

func switch_level(i):
	$Panel.visible = false
	$Main.level_resource = levels[i]
	$Main.reset_level(true)
	$Main.show()

func switch_to_level_select():
	$Main.hide()
	$Panel.visible = true