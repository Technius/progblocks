extends Node2D

const default_commands = [
	{
		"type": "ChangeVariable",
		"name": "collision_count",
		"action": 0, # Set
		"value": 0
	},
	{
		"type": "OnCollide",
		"action": {
			"type": "ChangeVariable",
			"name": "collision_count",
			"action": 1, # Add
			"value": 1
		}
	}
]

func _ready():
	pass