extends Node2D

var max_time = 20

const default_commands = [
	{
		"type": "ChangeVariable",
		"name": "count",
		"action": 0, # Set
		"value": 0
	},
	{
		"type": "Move",
		"impulse": Vector2(110, 170)
	},
	{
		"type": "OnCollide",
		"action": {
			"type": "ChangeVariable",
			"name": "count",
			"action": 1, # Add
			"value": 1
		}
	},
	{
		"type": "OnCollide",
		"action": {
			"type": "IfVariable",
			"name": "count",
			"criterion": 1, # Eq
			"value": 1,
			"action": {
				"type": "Move",
				"impulse": Vector2(-500, 130)
			}
		}
	}
]

func _ready():
	pass